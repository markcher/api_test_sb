baseCommand:
- python3 /parse_bis_rep/parse_aln_rep.py
class: CommandLineTool
cwlVersion: v1.0
id: PAR
inputs:
- id: ins
  inputBinding:
    position: 0
    shellQuote: false
  type:
    items: File
    type: array
label: Parse Alignment Report
outputs:
- id: out
  outputBinding:
    glob: alnRep.csv
  type: File
requirements:
- class: DockerRequirement
  dockerPull: eu-images.sbgenomics.com/universal-dx-training/parsealnrep:0.1.0
stdout: alnRep.csv
