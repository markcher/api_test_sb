baseCommand:
- echo
- HelloWorld
class: CommandLineTool
cwlVersion: v1.0
inputs: []
outputs:
- id: out
  outputBinding:
    glob: _stdout_
  type: File
requirements:
- class: DockerRequirement
  dockerPull: ubuntu:16.04
stdout: _stdout_
