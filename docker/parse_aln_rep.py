import csv
import itertools
import sys
import glob as gl

def parseFiles(files, bis):
    """
    Function that takes a list of bismark alignment reports
    files and returns a CSV with all the relevant stats.
    """
    filesdict = dict()
    for idx, f in enumerate(files):
        filedict = dict()
        with open(f, "r") as stream:
            for line in stream:
                for k, v in bis.items():
                    if v in line:
                        nv = line.split("\t")[1].strip()
                        filedict[k] = nv
        filesdict[f] = filedict

    fields = ["file"]
    for field in bis.keys():
        fields.append(field)
    w = csv.DictWriter(f = sys.stdout, fieldnames = fields, restval = "NA") 
    w.writeheader()
    for key, val in filesdict.items():
        row = {'file': key}
        row.update(val)
        w.writerow(row)


# make stats dict

bis = {"seq_pairs_tot" : "Sequence pairs analysed in total:",
    "paird_uniq_best" : "Number of paired-end alignments with a unique best hit:",
    "map_eff" : "Mapping efficiency:",
    "seq_pairs_no_aln" : "Sequence pairs with no alignments under any condition:",
    "seq_pairs_no_uniq_map" : "Sequence pairs did not map uniquely:",
    "seq_pairs_no_extract" : "Sequence pairs which were discarded because genomic sequence could not be extracted:",
    "CT_GA_CT" : "CT/GA/CT:",
    "GA_CT_CT" : "GA/CT/CT:",
    "GA_CT_GA" : "GA/CT/GA:",
    "CT_GA_GA" : "CT/GA/GA:",
    "tot_C" : "Total number of C's analysed:",
    "mCPG" : "Total methylated C's in CpG context:",
    "mCHG" : "Total methylated C's in CHG context:",
    "mCHH" : "Total methylated C's in CHH context:",
    "mCN" : "Total methylated C's in Unknown context:",
    "uCPG" : "Total unmethylated C's in CpG context:",
    "uCHG" : "Total unmethylated C's in CHG context:",
    "uCHH" : "Total unmethylated C's in CHH context:",
    "uCN" : "Total unmethylated C's in Unknown context:"}

if __name__ == "__main__":
    files = sys.argv[1:]
    parseFiles(files, bis)
